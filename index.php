<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta htttp-equiv="X-UA-Compatible" content="ie-edge">
  <title>BEZVERKHNII SPACE</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>

  <div class="container mt-4">
<?php
if(isset($_COOKIE['user']) == false):
?>
    <div class="row">

      <div class="col">
        <h1>Sign up</h1>
        <form action="validation-form\check.php" method="post">
          <input type="text" class="form-control" name="loginup" id="login" placeholder="Enter login"></br>
          <input type="text" class="form-control" name="nameup" id="name" placeholder="Enter name"></br>
          <input type="password" class="form-control" name="passup" id="pass" pattern=".{4,10}",".{a-z}" title="4 to 10 characters" placeholder="Enter password">
          <div class="col-auto">
    <span id="passwordHelpInline" class="form-text">
      Must be 4-10 characters long.
    </span>
  </div></br>
          <button class="btn btn-success" type="submit">Go</button>
        </Form>
      </div>

      <div class="col">
        <h1>Sign in</h1>
        <form action="validation-form\auth.php" method="post">
          <input type="text" class="form-control" name="login" id="login" placeholder="Enter your login"></br>
          <input type="password" class="form-control" name="pass" id="pass" placeholder="Enter your password"></br>
          <button class="btn btn-success" type="submit">Go</button>
        </Form>
      </div>
    <?php else: ?>
      <h6>Glory to Ukraine, <?=$_COOKIE['user']?>.</h6>
      <p>To exit click <a href="exit.php">here</a>.</p>
<?php endif;?>

    </div>

<!-- JavaScript -->
  <!-- </div>
  <script>
      $('myForm').validate({
        rules: {
          passup: {
            required: true,
            minlength: 4
          }
        }
      });
  </script> -->
</body>


</html>
